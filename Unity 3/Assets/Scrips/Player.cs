﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public CapsuleCollider playerCollider;
    public float MoveSpeed = 5f;

    //private GameObject enemy;
    private Enemy enemyScript;

    private RaycastHit hit;
    private Ray ray;
        public float rayDistance = 4f;
    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GetComponent<CapsuleCollider>();
        playerCollider.isTrigger = true;
        //playerCollider.center = new Vector3(0f, 0.5f, 0f);

        //enemy = GameObject.Find("BatGoblin");
        //enemyScript = enemy.GetComponent<Enemy>();

    }

    // Update is called once per frame
    void Update()
    {
        float MoveHorizontal = Input.GetAxis("Horizontal");
     
        float MoveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(MoveHorizontal, 0f, MoveVertical);
        transform.Translate(movement * Time.deltaTime * MoveSpeed);


        ray = new Ray(transform.position + new Vector3(0f, playerCollider.center.y , 0f) ,transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);


        if(Physics.Raycast(ray,out hit))
        {
            if( hit.distance < rayDistance)
            {
                if(hit.collider.gameObject.tag == "Enemy")
                {
                    print("There a enemy");
                }
                
                //print("We hit something!");
            }
        }

       /* if (Input.GetKeyDown(KeyCode.Space))
        {
            enemyScript.enemyHealth--;

        }
       */
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            enemyScript= collision.gameObject.GetComponent<Enemy>();
            enemyScript.enemyHealth--;
        }
    }

}
