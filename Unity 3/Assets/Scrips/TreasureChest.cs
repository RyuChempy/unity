﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour
{

    public bool interacting= false;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if ( interacting == true && Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("openChest", true);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag== "Player")
        {
            //print("Interacting");
            interacting = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag== "Player")
        {
            //print("Hey!");
            interacting = false;
        }
    }

}
