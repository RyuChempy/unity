﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example_B : MonoBehaviour
{
    // Start is called before the first frame update
    private int enemyDistance = 0;
    private int enemyCount = 0;

    private string[] enemies = new string[5];
    private int WeaponID = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //EnemySeach();
            //EnemyDestruction();
            //EnemyScan();
            //EnemyRoster();
            WeaponSearch();
        }
    }
    void EnemySeach()
    {
        for ( int i = 0; i > 5; i++)
        {
            enemyDistance = Random.Range(1, 10);

            if ( enemyDistance >= 8)
            {
                print("An enemy is far away");
            }
            if ( enemyDistance >= 4 && enemyDistance <= 7)
            {
                print("An enemy is a medium range");
            }
            if ( enemyDistance > 4)
            {
                print(" An enemy is very close by");
            }

        }
    }
    void EnemyDestruction()
    {
        while ( enemyCount > 0)
        {
            print("There is an enemy! Let's destroy it!");
            enemyCount--;
        }
    }
    void EnemyScan()
    {
        bool IsAlive = false;

        do
        {
            print("Scanning for enemies");


        }
        while ( IsAlive == true);

    }
    void EnemyRoster()
    {
        enemies[0] = "Orc";
        enemies[1] = "Drake";
        enemies[2] = "Snake";
        enemies[3] = "Ogre";
        enemies[4] = "Mage";

        foreach(string enemy in enemies)
        {
            print(enemy);
        }
    }
    void WeaponSearch()
    {
        WeaponID = Random.Range(1, 5);

        switch (WeaponID)
        {
            case 1:
                print("You found a sword");
                break;
            case 2:
                print("You found a bow");
                break;
            case 3:
                print("You found a spear");
                break;
            case 4:
                print("You found a shield");
                break;

            default:
                print("You didnt found anything");
                break;


        }
    }
}
